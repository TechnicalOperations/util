import queue
import pandas
import threading
from . import common
from . import query


def ssp_filter(deal, data):
    if deal.ssp_list is not None:
        ssp_cond = data['ssp_id'].isin(deal.ssp_list)
        if deal.include_ssp:
            data = data[ssp_cond]
        else:
            data = data[~ssp_cond]
    return data


def dsps_filter(deal, data):
    if deal.dsp_list is not None:
        data = data[data['dsps'].apply(lambda x: len(set(x) & set(deal.dsp_list)) > 0)]
    return data


# def bid_floor_filter(deal, data):
#     data = data[(data['bid_floor'] >= deal.bidfloor)]
#     return data


def media_type_filter(deal, data):
    media_types = []
    if deal.include_site:
        media_types.append('site')
    if deal.include_app:
        media_types.append('app')
    data = data[data['media_type'].isin(media_types)]
    return data


def imp_type_filter(deal, data):
    imp_types = []
    if deal.include_native:
        imp_types.append('native')
    if deal.include_video:
        imp_types.append('video')
    if deal.include_banner:
        imp_types.append('banner')
    data = data[data['imp_type'].isin(imp_types)]
    return data


def device_type_filter(deal, data):
    devices = []
    if deal.include_desktop:
        devices.append('desktop')
    if deal.include_mobile:
        devices.append('mobile')
    if len(devices) == 2:
        devices.append('unknown')
    data = data[data['device_type'].isin(devices)]
    return data


def country_filter(deal, data):
    if deal.country_list is None or len(deal.country_list) == 0:
        return data
    cond = data['country'].apply(lambda x: x.upper() if type(x) == str else False).isin(deal.country_list)
    if deal.include_country:
        data = data[cond]
    else:
        data = data[~cond]
    return data


def brand_safety_filter(deal, data):
    if deal.brand_safety is None:
        return data
    for k, v in deal.brand_safety.items():
        if k == 'IAS':
            data = data[(data['ias_rate'] <= v)]
        elif k == 'DV':
            data = data[(data['dv_rate'] <= v)]
    return data


def size_filter(deal, data):
    if deal.adsize_list is not None:
        adsize_cond = data['ad_size'].isin(deal.adsize_list)
        if not deal.include_adsize:
            adsize_cond = ~adsize_cond
    else:
        adsize_cond = data.apply(
            lambda x: True if x['imp_type'] == 'banner' else False,
            axis=1
        )
    if deal.videosize_list is not None:
        videosize_cond = data['video_size'].isin(deal.videosize_list)
        if not deal.include_videosize:
            videosize_cond = ~videosize_cond
    else:
        videosize_cond = data.apply(
            lambda x: True if x['imp_type'] == 'video' else False,
            axis=1
        )
    return data[adsize_cond | videosize_cond]


def adstxt_filter(deal, data):
    if deal.ads_txt == 'has_r1':
        return data[(data['adstxt'] == 'has_r1')]
    if deal.ads_txt == 'has_r1_or_missing':
        return data[data['adstxt'].isin(['has_r1', 'missing'])]
    return data


def viewability_filter(deal, data):
    if deal.viewability_percentage is None:
        return data
    for k, v in deal.viewability_percentage.items():
        data = data[(v <= data['viewability'])]
    return data


def segments_filter(deal, data):
    if deal.segment is None or len(deal.segment) == 0:
        return data
    exc_conds = []
    inc_conds = []
    for media_type, segments in deal.segment.items():
        for filter_type, groups in segments.items():
            cond = data['segments'].apply(lambda x: False)\
                if filter_type == 'include' else data['segments'].apply(lambda x: True)
            for group in groups.values():
                if filter_type == 'include':
                    cond &= data.apply(
                        lambda x: len(set(x['segments']) & group) == len(group) if x['media_type'] == media_type else True,
                        axis=1
                    )
                    inc_conds.append(cond)
                else:
                    cond &= data.apply(
                        lambda x: len(set(x['segments']) & group) == 0 if x['media_type'] == media_type else True,
                        axis=1
                    )
                    exc_conds.append(cond)
    final_cond = inc_conds[0]
    for c in inc_conds[1:]:
        final_cond |= c
    for c in exc_conds:
        final_cond &= c
    return data[final_cond]


EVAL_ORDER = [
    ('ssp_id', ssp_filter,),
    ('dsps', dsps_filter,),
    ('segments', segments_filter,),
    ('viewability', viewability_filter,),
    ('imp_type', imp_type_filter,),
    # ('bid_floor', bid_floor_filter,),
    ('media_type', media_type_filter,),
    ('device_type', device_type_filter,),
    ('country', country_filter,),
    ('brand_safety', brand_safety_filter,),
    ('size', size_filter,),
    ('adstxt', adstxt_filter,),
]


def evaluate_funnel_thread(data, input_queue, output_queue):
    while not input_queue.empty():
        deal_id, deal_config, = input_queue.get()
        row = {
            'deal_id': deal_id
        }
        filt = data
        for column, func in EVAL_ORDER:
            if len(filt) > 0:
                filt = func(deal_config, filt)
            if len(filt) > 0:
                row[column] = filt['requests'].sum()
                row['dsp_list'] = set([z for i in filt['dsps'] for z in i]) & deal_config.dsp_list
            else:
                row[column] = 0
                row['dsp_list'] = set()
        row['idx_list'] = set(filt.index.values)
        input_queue.task_done()
        output_queue.put(row)


def evaluate_competition(output, data, select=7):
    last = list(output.columns)
    last = last[len(last)-3]
    tmp = output[['deal_id', 'dsp_list', 'idx_list']].to_dict(orient='records')
    comp = []
    for row in tmp:
        for dsp in row['dsp_list']:
            for idx in row['idx_list']:
                comp.append(
                    {
                        'deal_id': row['deal_id'],
                        'dsp': dsp,
                        'idx': idx
                    }
                )
    del tmp
    comp = pandas.DataFrame(comp)[['deal_id', 'dsp', 'idx']]
    comp = comp.groupby(['idx', 'dsp']).agg(lambda x: set(x)).reset_index()
    comp = comp.merge(data[['requests']].reset_index(), left_on='idx', right_on='index', how='left')
    del comp['index']
    comp['probability'] = comp['deal_id'].apply(lambda x: 1 if len(x) < select else select / len(x))
    comp = comp[['idx', 'deal_id', 'requests', 'probability']].to_dict(orient='records')
    tmp = []
    for row in comp:
        for deal in row['deal_id']:
            tmp.append(
                {
                    'idx': row['idx'],
                    'deal_id': deal,
                    'requests': row['requests'],
                    'probability': row['probability']
                }
            )
    comp = pandas.DataFrame(tmp)
    comp = comp[['deal_id', 'idx', 'probability']].groupby(['deal_id', 'idx']).max().reset_index()
    comp = comp[['deal_id', 'probability']].groupby(['deal_id']).mean().reset_index()
    output = output.merge(comp, on='deal_id', how='left')
    cols = ['deal_id', 'total'] + [i[0] for i in EVAL_ORDER] + ['final_estimate']
    output['final_estimate'] = output[last] * output['probability']
    output['final_estimate'] = output['final_estimate'].fillna(0).astype(int)
    return output[cols]


def evaluate_funnel(deals, data, threads=10):
    output = []
    input_queue = queue.Queue()
    for k, v in deals.items():
        input_queue.put((k, v,))
    output_queue = queue.Queue()
    thread_list = []
    for i in range(threads):
        t = threading.Thread(target=evaluate_funnel_thread, args=(data, input_queue, output_queue,))
        t.start()
        thread_list.append(t)
    for t in thread_list:
        t.join()
    while not output_queue.empty():
        output.append(output_queue.get())
        output_queue.task_done()
    columns = ['deal_id', 'total'] + [i[0] for i in EVAL_ORDER] + ['dsp_list'] + ['idx_list']
    output = pandas.DataFrame(output)
    output['total'] = data['requests'].sum()
    output = output[columns]
    output = evaluate_competition(output, data)
    return output


def build_funnel():
    deals = common.get_deal_setup(None)
    data = query.get_filter_data(deals)
    funnel = evaluate_funnel(deals, data)
    return funnel
