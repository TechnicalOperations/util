from util import sql
from datetime import datetime
from datetime import timedelta
import json
import pandas


FILTER_ATTRIBUTES_QUERY = """
WITH adstxt_table AS (
  SELECT domain, MAX(has_r1_ads_txt::decimal(1, 0))::string AS has_r1_ads_txt
  FROM ods.tp.adstxt_crawler
  WHERE event_time >= '{start_date}'
    AND event_time < '{end_date}'
  GROUP BY 1
)
SELECT CASE WHEN B.has_r1_ads_txt = '1' THEN 'has_r1'
        WHEN B.has_r1_ads_txt IS NULL THEN 'missing'
        ELSE 'no_r1' END AS adstxt,
    CASE WHEN A.ad_size IN ('{adsizes}') THEN A.ad_size
        ELSE NULL END AS ad_size,
    IFF(video_w = '-', 'Unknown',
     IFF(TO_NUMERIC(video_w) <= 300, 'Small',
      IFF(TO_NUMERIC(video_w) <= 719, 'Medium',
       IFF(TO_NUMERIC(video_w) <= 2159, 'Large', 'Extra Large'))) 
    ) AS video_size,
    -- CASE {{floors}}
    --   ELSE 0.0 END AS bid_floor,
    CASE {ias_rate}
      ELSE NULL END AS ias_rate,
    CASE {dv_rate}
      ELSE NULL END AS dv_rate,
    IFF(country_code IN ('{countries}'), country_code, NULL) AS country,
    IFF(segments = '-',
        ARRAY_CONSTRUCT(),
        ARRAY_CONSTRUCT_COMPACT(
          {segments}
        )
       ) AS segments,
    CASE WHEN ias_viewability::string = '-' THEN NULL
        {viewability}
        ELSE NULL END AS viewability,
    media_type,
    imp_type,
    ssp_id,
    dsps,
    CASE WHEN device_type IN ('1', '4', '5') THEN 'mobile'
        WHEN device_type IN ('2') THEN 'desktop'
        WHEN device_type IN ('3', '6', '7') THEN 'connected'
        ELSE 'unknown' END AS device_type,
    SUM(A.requests) AS requests
FROM r1_edw.rx.bidrequest_d_est AS A
LEFT JOIN adstxt_table AS B ON A.site_domain=B.domain
WHERE A.event_date >= '{start_date}'
    AND A.event_date < '{end_date}'
    AND dsps RLIKE '.*{dsps}.*'
    AND request_fail IN ('bidresponse', '-', 'nodspbids')
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 -- , 14
"""


def build_attributes_query(deals):
    deals = list(deals.values())
    adsizes = [i.adsize_list for i in deals if i.adsize_list is not None]
    adsizes = set([z for i in adsizes for z in i])
    adsizes = "', '".join(adsizes)
    # floors = list(set([i.bidfloor for i in deals]))
    # floors.sort(key=lambda x: 0 - x)
    # floors = '\n      '.join(['WHEN bid_floor >= {0} THEN {0}'.format(i) for i in floors])
    ias_rate = set(
        [
            i.brand_safety['IAS'] for i in deals
            if i.brand_safety is not None and i.brand_safety.get('IAS', None) is not None
        ]
    )
    ias_rate = sorted(list(ias_rate))
    ias_rate = '\n      '.join(['WHEN ias_rate <= {0} THEN {0}'.format(i) for i in ias_rate])
    dv_rate = set(
        [
            i.brand_safety['DV'] for i in deals
            if i.brand_safety is not None and i.brand_safety.get('DV', None) is not None
        ]
    )
    dv_rate = sorted(list(dv_rate))
    dv_rate = '\n      '.join(['WHEN dv_rate <= {0} THEN {0}'.format(i) for i in dv_rate])
    countries = [i.country_list for i in deals if i.country_list is not None]
    countries = set([z for i in countries for z in i])
    countries = "', '".join(countries)
    segments = [list([(k, v,) for k, v in i.segment.items()]) for i in deals if i.segment is not None]
    segments = set([z[0].replace("'", "\\'") for i in segments for z in i])
    segments = ',\n          '.join(
        [
            "IFF(ARRAY_CONTAINS(LOWER('{0}')::variant, PARSE_JSON(segments)), '{0}', NULL)".format(i)
            for i in segments
        ]
    )
    viewability = set(
        [
            i.viewability_percentage['IAS'] for i in deals
            if i.viewability_percentage is not None and i.viewability_percentage.get('IAS', None) is not None
        ]
    )
    viewability = list(viewability)
    viewability.sort(key=lambda x: 0 - x)
    viewability = '\n        '.join([
        '''
        WHEN GREATEST(
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'adsize-country-domain')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'adsize-country-pubid-siteid-appid')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'pubid-adsize')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'score')::decimal(15, 5)),
            ZEROIFNULL(GET(PARSE_JSON(ias_viewability), 'sspid-pubid-siteid-appid')::decimal(15, 5))
        ) >= {0} THEN {0}'''.format(i)
        for i in viewability
    ])
    dsps = list(set([z for i in deals for z in i.dsp_list]))
    dsps = '.*|.*'.join(dsps)
    end_date = datetime.now()
    end_date = end_date.replace(hour=0, minute=0, second=0, microsecond=0)
    start_date = end_date - timedelta(days=1)
    return FILTER_ATTRIBUTES_QUERY.format(
        start_date=start_date,
        end_date=end_date,
        viewability=viewability,
        segments=segments,
        countries=countries,
        dv_rate=dv_rate,
        ias_rate=ias_rate,
        # floors=floors,
        adsizes=adsizes,
        dsps=dsps
    )


def get_filter_data(deals):
    query = build_attributes_query(deals)
    con = sql.connect()
    data = pandas.read_sql_query(query, con)
    sql.close()
    data = data.rename(columns={i: i.lower() for i in data.columns})
    segments = list(set(data['segments']))
    segments = {i: json.loads(i) for i in segments}
    data['segments'] = data['segments'].apply(lambda x: segments[x])
    dsps = list(set(data['dsps']))
    dsps = {i: json.loads(i) for i in dsps}
    data['dsps'] = data['dsps'].apply(lambda x: dsps[x])
    return data
