import json
import MySQLdb


AUTH = {
    'mysql': '/Users/cbeecher/.auth/.my.conf-rx'
}


DEAL_SETUP = """
SELECT A.external_deal_id,
	A.description,
    D.type,
    GROUP_CONCAT(C.name SEPARATOR ',') AS dsps,
    GROUP_CONCAT(H.name SEPARATOR ',') AS ssps,
    GROUP_CONCAT(DISTINCT CONCAT_WS(';;;', F.name, E.viewability_percentage)) AS viewability_percentage,
    GROUP_CONCAT(DISTINCT CONCAT_WS(';;;', J.provider, J.score_threshold)) AS brand_safety,
    GROUP_CONCAT(DISTINCT CONCAT_WS(';;;', L.export_name, K.media_type, K.filter_type, K.group_id)) AS segment,
    A.bidfloor,
    A.filter_ssp,
    A.filter_country,
    A.filter_countrylist,
    A.filter_adsize,
    A.filter_adsize_list,
    A.filter_videosize,
    A.filter_videosize_list,
    A.filter_banner,
    A.filter_video,
    A.filter_native,
    A.filter_display AS filter_desktop,
    A.filter_mobile,
    A.filter_site,
    A.filter_app,
    CASE WHEN A.filter_ads_txt = 1 THEN 'has_r1'
		WHEN A.filter_ads_txt = 2 THEN 'has_r1_or_missing'
        ELSE null END AS ads_txt,
    A.wseat,
    A.filter_inviewpercent
FROM deal AS A
LEFT JOIN deal_dsp AS B ON A.deal_id=B.deal_id
LEFT JOIN dsp AS C ON B.dsp_id=C.id
LEFT JOIN deal_type AS D ON A.type=D.id
LEFT JOIN deal_viewability_filter AS E ON A.deal_id=E.deal_id
LEFT JOIN viewability_provider AS F ON E.viewability_provider_id=F.id
LEFT JOIN deal_ssp AS G ON A.deal_id=G.deal_id
LEFT JOIN ssp AS H ON G.ssp_id=H.id
LEFT JOIN deal_safety_filter AS I ON A.deal_id=I.deal_id
LEFT JOIN safety_filter AS J ON I.safety_filter_id=J.id
LEFT JOIN deal_segment AS K ON A.deal_id=K.deal_id
LEFT JOIN segment AS L ON K.segment_id=L.segment_id
WHERE A.active = 1
    AND DATE_SUB(NOW(), INTERVAL 1 DAY) BETWEEN deal_start AND deal_end{0}
GROUP BY A.external_deal_id,
	A.description,
    D.type,
    A.bidfloor,
    A.filter_ssp,
    A.filter_country,
    A.filter_countrylist,
    A.filter_adsize,
    A.filter_adsize_list,
    A.filter_videosize,
    A.filter_videosize_list,
    A.filter_banner,
    A.filter_video,
    A.filter_native,
    A.filter_display,
    A.filter_mobile,
    A.filter_site,
    A.filter_app,
    ads_txt,
    A.wseat,
    A.filter_inviewpercent
"""


class Deal:

    def __init__(self, row):
        self.deal_id = row[0]
        self.description = row[1]
        self.deal_type = row[2]
        self.dsp_list = set([i.strip() for i in row[3].split(',')])
        if '' in self.dsp_list:
            self.dsp_list.remove('')
        if row[4] is not None:
            self.ssp_list = set([i.strip() for i in row[4].split(',')])
            if '' in self.ssp_list:
                self.ssp_list.remove('')
        else:
            self.ssp_list = None
        if row[5] is not None:
            self.viewability_percentage = [z.split(';;;') for z in row[5].split(',')]
            self.viewability_percentage = {z[0]: float(z[1]) for z in self.viewability_percentage if len(z) == 2}
        else:
            self.viewability_percentage = None
        if row[6] is not None:
            self.brand_safety = [z.split(';;;') for z in row[6].split(',')]
            self.brand_safety = {z[0]: float(z[1]) for z in self.brand_safety if len(z) == 2}
        else:
            self.brand_safety = None
        if row[7] is not None:
            temp_segment = [z.split(';;;') for z in row[7].split(',')]
            self.segment = {}
            for segment_row in temp_segment:
                if len(segment_row) < 4:
                    continue
                if segment_row[1] not in self.segment:
                    self.segment[segment_row[1]] = {}
                media = self.segment[segment_row[1]]
                if segment_row[2] not in media:
                    media[segment_row[2]] = {}
                filter_type = media[segment_row[2]]
                if segment_row[3] not in filter_type:
                    filter_type[segment_row[3]] = {segment_row[0]}
                else:
                    filter_type[segment_row[3]].add(segment_row[0])
        else:
            self.segment = None
        self.bidfloor = float(row[8]) if row[8] is not None else None
        self.include_ssp = False if row[9] == 'exclude' else True
        self.include_country = True if row[10] == 'include' else False
        self.country_list = None if row[11] is None else set(
            [k for k, v in json.loads(row[11]).items() if v == 1]
        )
        self.include_adsize = True if row[12] == 'include' else False
        self.adsize_list = None if row[13] is None else set(
            [k for k, v in json.loads(row[13]).items() if v == 1]
        )
        self.include_videosize = True if row[14] == 'include' else False
        self.videosize_list = None if row[15] is None else set(
            [k for k, v in json.loads(row[15]).items() if v == 1]
        )
        self.include_banner = True if row[16] == 1 else False
        self.include_video = True if row[17] == 1 else False
        self.include_native = True if row[18] else False
        self.include_desktop = True if row[19] else False
        self.include_mobile = True if row[20] else False
        self.include_site = True if row[21] else False
        self.include_app = True if row[22] else False
        self.ads_txt = row[23]
        self.wseat = row[24]
        self.include_inviewpercent = int(row[25]) if row[25] is not None else None


def get_deal_setup(deal_ids):
    """
    :param deal_ids: String array of deals
    :return: Deal class dictionary
    """
    con = MySQLdb.connect(read_default_file=AUTH['mysql'], db='dashboard')
    cur = con.cursor()
    if deal_ids is None or len(deal_ids) == 0:
        f = ''
    else:
        f = """
 	AND A.external_deal_id IN (
 		'{0}'
     )""".format("',\n'".join(deal_ids))
    cur.execute(DEAL_SETUP.format(f))
    deals = cur.fetchall()
    con.close()
    deals = {i[0]: Deal(i) for i in deals}
    return deals


if __name__ == '__main__':
    deals = get_deal_setup(None)
    print(deals['1R-RX1-96612-20180123940730'].segment)
