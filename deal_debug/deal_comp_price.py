import json
import pandas
from util import sql
from datetime import datetime
from datetime import timedelta
pandas.set_option('display.width', 5000)


DEAL_COMPETITION = """
SELECT DATE_TRUNC(DAY, A.event_time_est) AS date,
    C.value::string AS deal_id,
    A.dsp_id,
    ARRAY_AGG(DISTINCT IFF(B.deal_id = C.value::string, NULL, B.deal_id)) AS res_deal_id,
    SUM(A.sample_rate) AS total_requests,
    SUM(IFF(B.deal_id != C.value::string AND B.deal_id IS NOT NULL AND B.deal_id != '', B.sample_rate, 0)) AS lost_other_deal,
    SUM(IFF(B.deal_id IS NULL OR B.deal_id = '', B.sample_rate, 0)) AS lost_other_io,
    SUM(B.sample_rate) AS total_responses,
    AVG(IFF(B.deal_id = C.value::string, B.dsp_price, NULL)) AS deal_avg_bid_price,
    STDDEV(IFF(B.deal_id = C.value::string, B.dsp_price, NULL)) AS deal_stddev_bid_price,
    AVG(B.dsp_price) AS total_avg_bid_price,
    STDDEV(B.dsp_price) AS total_stddev_bid_price
FROM ods.rx.dspbidrequest AS A
LEFT JOIN ods.rx.bidresponse AS B ON A.request_id=B.request_id
    AND B.event_time_est >= '{start_date}'
    AND B.event_time_est < '{end_date}',
LATERAL FLATTEN(INPUT=>PARSE_JSON(deal_ids)) C
WHERE A.event_time_est >= '{start_date}'
    AND A.event_time_est < '{end_date}'
    AND A.deal_ids IS NOT NULL
    AND A.deal_ids != ''
    AND ARRAY_SIZE(PARSE_JSON(A.deal_ids)) > 0
GROUP BY 1, 2, 3
"""


def get_deal_competition_metrics(look_back_days=1):
    end = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    start = end - timedelta(days=look_back_days)
    con = sql.connect()
    data = pandas.read_sql_query(DEAL_COMPETITION.format(end_date=end, start_date=start), con)
    con.close()
    data = data.rename(columns={i: i.lower() for i in data.columns})
    data['total_requests'] = data['total_requests'].fillna(0).astype(int)
    data['lost_other_deal'] = data['lost_other_deal'].fillna(0).astype(int)
    data['lost_other_io'] = data['lost_other_io'].fillna(0).astype(int)
    data['total_responses'] = data['total_responses'].fillna(0).astype(int)
    data['res_deal_id'] = data['res_deal_id'].apply(lambda x: json.loads(x))
    data = data[data['res_deal_id'].apply(lambda x: len(x) > 0)]
    data['res_deal_id'] = data['res_deal_id'].apply(lambda x: ','.join(x))
    data = data[data['res_deal_id'].apply(lambda x: x.strip() != '')]
    data = data.reset_index(drop=True)
    return data
