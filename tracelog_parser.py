import re
import json


sections = re.compile('#{2,}[^#]+?#{2,}')
time = re.compile('\(.*?\)')
valid_key = re.compile('[a-zA-Z0-9 ]')


def iter_stack(string):
    out = ['{']
    stack = 1
    idx = 1
    while stack > 0:
        if string[idx] == '{':
            stack += 1
        elif string[idx] == '}':
            stack -= 1
        out.append(string[idx])
        idx += 1
    return idx, ''.join(out)


def find_object_key(string, index):
    out = []
    while string[index] != ':':
        index -= 1
    index -= 1
    while string[index] != '\n' and index > 0:
        if valid_key.search(string[index]) is not None:
            out.append(string[index])
        index -= 1
    out = reversed(out)
    return ''.join(out).strip()


def find_json_objects(string):
    items = {}
    i = 0
    while i < len(string):
        c = string[i]
        if c == '{':
            key = find_object_key(string, i)
            inc, item = iter_stack(string[i:])
            i += inc
            try:
                item = json.loads(item)
                if key not in items:
                    items[key] = []
                items[key].append(item)
            except json.JSONDecodeError:
                pass
        else:
            i += 1
    if 'server raw result' in items:
        items['server raw result'] = items['server raw result'][0]
        x = items['server raw result']['ext']['profile']
        x = [v for k, v in x.items() if 'place' in k]
        items['server raw result'] = x
    return items


def clean_trace(trace):
    return trace.replace('&quot;', '"')


def find_sections(trace):
    s = sections.findall(trace)
    s = [time.sub('', i).replace('#', '').strip().lower() for i in s]
    b = sections.split(trace)
    out = {}
    b = b[1:]
    for i in range(len(s)):
        if s[i] not in out:
            out[s[i]] = []
        out[s[i]].append(b[i])
    return out


def parse_select_ad(section):
    pass
