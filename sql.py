import os
import pandas
import pickle
from datetime import datetime
from snowflake import connector
from os.path import expanduser
pandas.set_option('display.width', 5000)


param_map = {
    'accountname': 'account',
    'username': 'user',
    'password': 'password',
    'dbname': 'database',
    'rolename': 'role',
}

con = None


def read_params():
    with open(expanduser('~')+'/.snowsql/config', 'r') as f:
        params = f.read()
    params = [i.split('=') for i in params.split('\n')]
    params = {param_map[i[0]]: i[1] for i in params if i[0] in param_map}
    return params


def connect():
    global con
    params = read_params()
    con = connector.connect(**params)
    return con


def close():
    global con
    con.close()


def get_data(query, tmp_file=None, date=None):
    con = connect()
    try:
        data = pandas.read_sql_query(query, con)
    except (connector.errors.ProgrammingError, pandas.io.sql.DatabaseError) as err:
        con.close()
        raise err
    close()
    data = data.rename(columns={i: i.lower() for i in data.columns})
    if tmp_file is not None:
        with open(tmp_file.replace('~', os.path.expanduser('~')), 'w') as f:
            f.write(pickle.dumps(data))
    if date is not None:
        data['date'] = data['date'].apply(lambda x: datetime(year=x.year, month=x.month, day=x.day))
    return data
