import os
import json
import pandas
import random
import argparse
import requests
from copy import deepcopy


DEFAULT_OUTPUT = "output.csv"
headers = {'Content-type': 'application/json', 'x-openrtb-version': '2.3', 'Connection': 'keep-alive'}

alpha = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.')


def apply_config(request, config):
    request = deepcopy(request)
    for k, v in config.items():
        if k not in request:
            continue
        if type(request[k]) == dict and type(v) == dict:
            request[k] = apply_config(request[k], v)
            continue
        elif type(request[k]) == dict and type(v) != dict:
            raise ValueError(
                "Mismatch data types in configuration.  Unexpected type {0} in config key {1}".format(type(v), k)
            )
        if type(request[k]) == list and type(v) == list:
            request[k] = deepcopy(v)
            continue
        elif type(request[k]) == list and type(v) != list:
            raise ValueError(
                "Mismatch data types in configuration.  Unexpected type {0} in config key {1}".format(type(v), k)
            )
        if type(v) == list:
            request[k] = random.choice(v)
        else:
            request[k] = v
    return request


def ping(
        inp,
        endpoint,
        config=None,
        output=DEFAULT_OUTPUT,
        trace=False,
        single=False
        ):
    """
    :param inp: Requests.  Can either be an array or a string that points to a json object file
    :param endpoint:  The endpoint to ping.  Requests are sent as POST with the body being a JSON string
    :param config:  A string pointing to a JSON file with request default values and changes
    :param output:  Where to output the results as a CSV
    :param trace:  Boolean to indicate whether to append "&trace=true" to each request
    :param single:  Boolean to only ping the endpoint once
    :return:  The same return as output, but the data type is a pandas DataFrame
    """

    if endpoint is None:
        print("Endpoint not specified")
        exit()
    if inp is None:
        print("Input not specified")
        exit()
    if config is not None:
        config = config.replace('~', os.path.expanduser('~'))
        with open(config, 'r') as f:
            config = json.loads(f.read())

    if type(inp) == str:
        inp = inp.replace('~', os.path.expanduser('~'))
        inp = pandas.read_csv(inp)
    else:
        reqs = inp
        inp = pandas.DataFrame(columns=['request'])
        inp['request'] = reqs
        del reqs
    reqs = list(inp[list(inp.columns)[0]])
    reqs = [json.loads(i) for i in reqs]
    endpoint = endpoint+'&trace=true' if trace or single else endpoint
    if single:
        if config is not None:
            reqs[0] = apply_config(reqs[0], config)
        res = requests.post(endpoint, json=reqs[0], headers=headers)
        inp = (reqs[0], res.text,)
        with open(output, 'w') as f:
            f.write(res.text)
    else:
        res = []
        r = requests.Session()
        for req in reqs:
            if config is not None:
                req = apply_config(reqs, config)
            p = r.post(endpoint, json=req, headers=headers)
            res.append((p.status_code, p.text,))
        r.close()
        inp['status_code'] = [i[0] for i in res]
        inp['response_text'] = [i[1] for i in res]
        inp.to_csv(output, index=False)
    print("Wrote output to {0}".format(output))
    print("Received {0} 200 responses".format(len(inp[(inp['status_code'] == 200)])))
    return inp


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="JSON file to be used as requests", type=str)
    parser.add_argument("--config", help="JSON file to change base values of requests", type=str)
    parser.add_argument("--endpoint", help="Endpoint to be pinged", type=str)
    parser.add_argument("--output", help="File to store the results", type=str, default=DEFAULT_OUTPUT)
    parser.add_argument("--trace", help="Indicate whether to trace ping (true or false", type=str, default="false")
    parser.add_argument("--single",
                        help="Ping the endpoint with a single request and store the trace (true or false)",
                        type=str, default="false")
    args = parser.parse_args()

    inp = args.input
    config = args.config
    endpoint = args.endpoint
    output = args.output
    trace = True if args.trace.lower() == "true" else False
    single = True if args.single.lower() == "true" else False
    ping(inp, endpoint, config=config, output=output, trace=trace, single=single)
