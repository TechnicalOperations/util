import json
import pandas
from util import sql
from datetime import datetime, timedelta
pandas.set_option('display.width', 5000)

now = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
DEFAULT_START = now - timedelta(days=7)
DEFAULT_END = now
del now


NO_DSP_QUERY = """
    SELECT event_date AS date,
        site_id,
        dsps,
        SUM(IFF(dsps = '-' OR ARRAY_SIZE(PARSE_JSON(dsps)) = 0, requests, 0)) AS no_dsp,
        SUM(requests) AS requests
    FROM r1_edw.rx.bidrequest_d_est
    WHERE event_date >= '{start}'
        AND event_date < '{end}'
        AND ssp_id LIKE 'rmp%'
        AND site_id IS NOT NULL
        AND (request_fail IS NULL OR request_fail IN ('nodsp', 'bidresponse'))
    GROUP BY date, site_id, dsps
"""


FILTER_REASON_QUERY = """
    WITH dsps AS (
      SELECT A.site_id, B.value AS dsp
      FROM ods.rx.bidrequest AS A,
      LATERAL FLATTEN(input=>PARSE_JSON(dsps)) B
      WHERE event_time_est >= '{start}'
        AND event_time_est < '{end}'
        AND ssp_id LIKE 'rmp%'
        AND site_id IS NOT NULL
        AND (request_fail IS NULL OR request_fail IN ('nodsp', 'bidresponse'))
      GROUP BY 1, 2
    ),
    filters AS (
      SELECT DATE_TRUNC(DAY, A.event_time_est) AS date,
          A.site_id,
          A.request_id,
          B.key::string AS dsp,
          B.value::string AS filter_reason,
          A.sample_rate
      FROM ods.rx.bidrequest AS A,
      LATERAL FLATTEN(input=>dsp_filter) B
      WHERE event_time_est >= '{start}'
          AND event_time_est < '{end}'
          AND ssp_id LIKE 'rmp%'
          AND site_id IS NOT NULL
          AND (request_fail IS NULL OR request_fail IN ('nodsp', 'bidresponse'))
    ),
    jnd AS (
       SELECT A.request_id,
            A.date,
            A.site_id,
            OBJECT_AGG(A.dsp, A.filter_reason::variant) AS filter_reasons,
            A.sample_rate
       FROM filters AS A
       INNER JOIN dsps AS B ON A.site_id=B.site_id AND A.dsp=B.dsp
       GROUP BY A.request_id, A.date, A.site_id, A.sample_rate
    )
    SELECT date, site_id, filter_reasons, SUM(sample_rate) AS requests
    FROM jnd
    GROUP BY date, site_id, filter_reasons
"""


def get_no_dsp(start=DEFAULT_START, end=DEFAULT_END, input_file=None, reset=False):
    try:
        if input_file is not None and not reset:
            data = pandas.read_csv(input_file)
        else:
            raise FileNotFoundError
        dates = set(data['date'])
        dates = {
            i: datetime.strptime(i[:10], '%Y-%m-%d') for i in dates
        }
        data['date'] = data['date'].apply(lambda x: dates[x])
        data['site_id'] = data['site_id'].fillna('').astype(str)
        data['no_dsp'] = data['no_dsp'].fillna(0).astype(int)
        data['requests'] = data['requests'].fillna(0).astype(int)
    except (FileNotFoundError, OSError) as err:
        q = NO_DSP_QUERY.format(start=start, end=end)
        con = sql.connect()
        data = pandas.read_sql_query(q, con)
        sql.close()
        data = data.rename(columns={i: i.lower() for i in data.columns})
        dsps = set(data['dsps'])
        dsps = {i: json.dumps(sorted(list(set(json.loads(i))))) for i in dsps if i != '-'}
        data['dsps'] = data['dsps'].apply(lambda x: dsps.get(x, "[]"))
        data['no_dsp'] = data['no_dsp'].fillna(0).astype(int)
        data['requests'] = data['requests'].fillna(0).astype(int)
        data['date'] = data['date'].apply(lambda x: datetime(year=x.year, month=x.month, day=x.day))
        if input_file is not None:
            data.to_csv(input_file, index=False)
    dsps = set(data['dsps'])
    dsps = {dsp: json.loads(dsp) for dsp in dsps}
    data['dsps'] = data['dsps'].apply(lambda x: dsps[x])
    data = data[(data['site_id'] != '-')].reset_index(drop=True)
    data['site_id'] = data['site_id'].astype(str)
    return data


def get_filter_reason(start=DEFAULT_START, end=DEFAULT_END, input_file=None, reset=False):
    try:
        if input_file is not None and not reset:
            data = pandas.read_csv(input_file)
        else:
            raise FileNotFoundError
        dates = set(data['date'])
        dates = {i: datetime.strptime(i[:10], '%Y-%m-%d') for i in dates}
        data['date'] = data['date'].apply(lambda x: dates[x])
        data['requests'] = data['requests'].fillna(0).astype(int)
    except (FileNotFoundError, OSError) as err:
        q = FILTER_REASON_QUERY.format(start=start, end=end)
        con = sql.connect()
        data = pandas.read_sql_query(q, con)
        sql.close()
        data = data.rename(columns={i: i.lower() for i in data.columns})
        data['requests'] = data['requests'].fillna(0).astype(int)
        if input_file is not None:
            data.to_csv(input_file, index=False)
    reasons = set(data['filter_reasons'])
    reasons = {i: json.loads(i) for i in reasons}
    data['filter_reasons'] = data['filter_reasons'].apply(
        lambda x: tuple(sorted([(k, v,) for k, v in reasons[x].items()]))
    )
    data['site_id'] = data['site_id'].astype(str)
    data = data.groupby(['date', 'site_id', 'filter_reasons']).sum().reset_index()
    # data['filter_reasons'] = data['filter_reasons'].apply(lambda x: list(x))
    return data
