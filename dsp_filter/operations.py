import json
import pandas
import numpy as np
from util import stats
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler


NO_DSP_STRING = 'NO_DSP'


#############################
def order_set_list_json(row):
    row = list(set(row))
    row.sort()
    return json.dumps(row)


def site_dsp_slope(site):
    lr = LinearRegression()
    row = site.reset_index()[['date', 'avg_dsp']].copy()
    row['date'] = row['date'].apply(lambda x: x.timestamp())
    values = row.values
    lr.fit(values[:, 0:1], values[:, 1:2])
    site['avg_dsp_regression'] = lr.predict(values[:, 0:1])[:, 0]
    ss = StandardScaler()
    ss.fit(values[:, 0:1], values[:, 1:2])
    new_values = ss.transform(values[:, 0:1])
    lr.fit(new_values[:, 0:1], values[:, 1:2])
    slope = lr.coef_[0][0]
    angle = np.arctan(slope) * 180 / np.pi
    site['slope_angle'] = angle

    lr = LinearRegression()
    row = site.reset_index()[['date', 'requests']].copy()
    row['date'] = row['date'].apply(lambda x: x.timestamp())
    values = row.values
    ss = StandardScaler()
    ss.fit(values[:, 0:1], values[:, 1:2])
    new_values = ss.transform(values[:, 0:1])
    lr.fit(new_values[:, 0:1], values[:, 1:2])
    slope = lr.coef_[0][0]
    angle = np.arctan(slope) * 180 / np.pi
    site['requests_angle'] = angle
    return site


def site_no_dsp_slope(site):
    lr = LinearRegression()
    ss = StandardScaler()
    row = site.reset_index()[['date', 'no_dsp']].copy()
    row['date'] = row['date'].apply(lambda x: x.timestamp())
    values = row.values
    ss.fit(values[:, 0:1], values[:, 1:2])
    new_values = ss.transform(values[:, 0:1])
    lr.fit(new_values[:, 0:1], values[:, 1:2])
    site['no_dsp_regression'] = lr.predict(new_values[:, 0:1])[:, 0]
    slope = lr.coef_[0][0]
    site['no_dsp_angle'] = np.arctan(slope) * 180 / np.pi

    row = site.reset_index()[['date', 'no_dsp', 'requests']].copy()
    row['no_dsp_percent_angle'] = row['no_dsp'] * 100.0 / row['requests']
    del row['no_dsp']
    del row['requests']
    row['date'] = row['date'].apply(lambda x: x.timestamp())
    values = row.values
    ss.fit(values[:, 0:1], values[:, 1:2])
    new_values = ss.transform(values[:, 0:1])
    lr.fit(new_values[:, 0:1], values[:, 1:2])
    slope = lr.coef_[0][0]
    site['no_dsp_percent_angle'] = np.arctan(slope) * 180 / np.pi
    return site


def site_dsp_stats(no_dsp):
    dsps = no_dsp[['date', 'site_id', 'dsps', 'requests']].copy()
    no_dsp = no_dsp[['date', 'site_id', 'no_dsp', 'requests']].groupby(['date', 'site_id']).sum()
    no_dsp['no_dsp_regression'] = 0.0
    no_dsp['no_dsp_angle'] = 0.0
    no_dsp['no_dsp_percent_angle'] = 0.0
    no_dsp = no_dsp.groupby(level=1).apply(site_no_dsp_slope)
    del no_dsp['requests']
    dsps['dsp_count'] = dsps['dsps'].apply(len)
    dsps['dsps'] = dsps['dsps'].apply(order_set_list_json)
    distinct = dsps[['date', 'site_id', 'dsps']].groupby(['date', 'site_id'])
    distinct = distinct.agg(lambda x: len(set(x))).rename(columns={'dsps': 'distinct_dsps'})
    del dsps['dsps']
    dsps = stats.weighted_stats(dsps, ['date', 'site_id'], 'dsp_count', 'requests')
    # del dsps['min']
    # del dsps['max']
    dsps['min'] = dsps['min'].astype(int)
    dsps['max'] = dsps['max'].astype(int)
    stats_cols = ['avg', 'std', 'min', 'max']
    dsps = dsps.rename(columns={i: i + '_dsp' for i in stats_cols})
    dsps = dsps.merge(distinct, left_index=True, right_index=True, how='left')
    dsps['slope_angle'] = 0.0
    dsps['avg_dsp_regression'] = 0.0
    dsps['requests_angle'] = 0.0
    dsps = dsps.groupby(level=1).apply(site_dsp_slope)
    dsps = dsps.merge(no_dsp, left_index=True, right_index=True, how='left')
    dsps = dsps[
        [
            'slope_angle',
            'avg_dsp_regression',
            'avg_dsp',
            'std_dsp',
            'min_dsp',
            'max_dsp',
            'distinct_dsps',
            'no_dsp_regression',
            'no_dsp_angle',
            'no_dsp_percent_angle',
            'no_dsp',
            'requests_angle',
            'requests'
         ]
    ]
    dsps = dsps.reset_index()
    return dsps


def determine_impacted_site_ids(site_dsps, min_req_one_day=0):
    impact = site_dsps[
        (site_dsps['slope_angle'] < 0.0) &
        (site_dsps['no_dsp_angle'] > 0.0) &
        (site_dsps['no_dsp_percent_angle'] > 0.0) &
        (site_dsps['requests_angle'] > 0.0) &
        (site_dsps['max_dsp'] < site_dsps['avg_dsp'] + site_dsps['std_dsp']) &
        (site_dsps['min_dsp'] > site_dsps['avg_dsp'] + site_dsps['std_dsp']) &
        (site_dsps['requests'] >= min_req_one_day)
        ]
    impact = impact[['site_id', 'slope_angle', 'requests']].groupby(['site_id', 'slope_angle'])
    impact = impact.sum().reset_index().sort_values(by='slope_angle', ascending=True)
    return list(impact['site_id'])


def filter_correlation(site):
    row = site.reset_index()
    reasons = list(set(row['filter_reasons']))
    reasons = {reasons[i]: i for i in range(len(reasons))}
    row['filter_reasons'] = row['filter_reasons'].apply(lambda x: reasons[x])
    row = pandas.pivot_table(
        row,
        index=['site_id', 'date'],
        columns=['filter_reasons'],
        aggfunc={
            'requests': lambda x: int(np.sum(x))
        },
        fill_value=0
    )
    row = row.corr()
    row = row[~row.index.get_level_values(1).isin([reasons[NO_DSP_STRING]])][('requests', reasons[NO_DSP_STRING],)]
    row = row.to_frame()
    row.columns = row.columns.droplevel(0)
    row = row.rename(columns={reasons[NO_DSP_STRING]: 'no_dsp_corr'})
    row.index = row.index.droplevel(0)
    row = row.reset_index()
    reasons = {v: k for k, v in reasons.items()}
    row['filter_reasons'] = row['filter_reasons'].apply(lambda x: reasons[x])
    return row


def site_filter_reasons(filters, no_dsp, impacted):
    no_dsp = no_dsp[['date', 'site_id', 'no_dsp']].groupby(['date', 'site_id']).sum().reset_index()
    no_dsp['filter_reasons'] = NO_DSP_STRING
    no_dsp = no_dsp.rename(columns={'no_dsp': 'requests'})
    data = pandas.concat([filters, no_dsp])
    data = data[data['site_id'].isin(impacted)].reset_index(drop=True)
    data = data.groupby(['date', 'site_id', 'filter_reasons']).sum()
    data = data.groupby(level=1).apply(filter_correlation)
    data = data.reset_index(level=1, drop=True).reset_index()
    return data


def find_common(items):
    items = list(items)
    comm = set(items[0])
    for item in items[1:]:
        comm &= set(item)
    comm = list(comm)
    comm.sort()
    return tuple(comm)


def primary_filter_reasons(site):
    row = site.reset_index()
    row = row.sort_values(by='no_dsp_corr', ascending=False)
    commons = [find_common(row['filter_reasons'][:i+1]) for i in range(len(row))]
    row['filter_reasons'] = commons
    row = row[row['filter_reasons'].apply(lambda x: len(x) > 0)]
    row = row[['site_id', 'filter_reasons']].groupby(['site_id', 'filter_reasons']).size()
    row = row.reset_index(level=1)
    del row[0]
    row = row.rename(columns={'filter_reasons': 'common_reasons'})
    return row


def determine_primary_filter_reasons(reasons):
    reasons = reasons.groupby(['site_id', 'filter_reasons']).mean()
    reasons = reasons.groupby(level=0).apply(primary_filter_reasons)
    reasons = reasons.reset_index(level=1, drop=True)
    return reasons.reset_index()
